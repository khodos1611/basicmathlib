package org.hillel.basic.maths;

public class BasicMathUtils {

     public double getSqrt(double source){
         return Math.sqrt(source);
     }

     public double getExp(double source){
         return Math.getExponent(source);
     }

     public double getRandom(){
         return Math.random();
     }

    public int getSum(int a, int b){
        return Math.addExact(a,b);
    }
}
