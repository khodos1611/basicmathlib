package org.hillel.basic.maths;

import org.junit.jupiter.api.Test;

public class BasicMathUtilsTest {

    @Test
    void testBasicMathUtils(){
        BasicMathUtils basicMathUtils = new BasicMathUtils();

        System.out.println("SQRT = " + basicMathUtils.getSqrt(56.235));

        System.out.println("EXP = " + basicMathUtils.getExp(25.0));

        System.out.println("random = " + basicMathUtils.getRandom());

        System.out.println("Add = " + basicMathUtils.getSum(6,78));

    }
}
