package org.hillel.service;

import org.hillel.basic.maths.BasicMathUtils;
import org.hillel.services.PropertyReader;

import java.io.IOException;

public class TextProvider {

    private static final PropertyReader propertyReader = new PropertyReader();
    private static final BasicMathUtils basicMathUtils = new BasicMathUtils();

    public static String getText(double source) throws IOException {


        //return propertyReader.getProperties("test.properties").getProperty("mmm", "Hey");

        return  Double.toString(basicMathUtils.getSqrt(source));
    }
}
